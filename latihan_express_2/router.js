import express from "express";
import {mainRouter} from './controller/basicController.js '
import { basicMiddleware } from "./middleware/basicMiddleware.js";
import productApi from './api/productAPI.js'
import { biodata } from "./controller/biodataController.js";
import orderApi from './api/orderAPI.js'
// import { get as getProduct, create as createProcuct, 
//     update as updateProduct, destroy as destroyproduct,
//     find as findProduct} from "./controller/produkController.js";

const router = express.Router()


router.get('/check-health', mainRouter)
router.get('/biodata', biodata) // disini letak menaruh middle ware, di sebelum secondRouter
router.use('/product',  productApi)
router.use('/order',  orderApi)

export {router}