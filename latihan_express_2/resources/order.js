const orders= []

class Order {
    constructor(params)
    {
        this.id = params.id;
        this.name = params.name;
        this.food = params.food;
        this.price = params.price;
    }

    static list(params)
    {
        return orders;
    }

    static create(params)
    {
        const order = new this(params)

        orders.push(order)

        return order;
    }

    static find(id)
    {
        const order = orders.find((i) => i.id === Number(id));
        if (!order) 
            return null;
        
        return order;
        
    }
}

export default Order